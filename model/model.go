package model

type modelError string

func (m modelError) Error() string {
	return string(m)
}
