package model

import (
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
)

var (
	ErrUsernameRequired     modelError = "ERR_USERNAME_REQUIRED"
	ErrPasswordRequired     modelError = "ERR_PASSWORD_REQUIRED"
	ErrPasswordMismatch     modelError = "ERR_PASSWORD_MISMATCH"
	ErrUseranmeAlreadyTaken modelError = "ERR_USERNAME_TAKEN"
)

type NewUser struct {
	Username        string `json:"username,omitempty"`
	Password        string `json:"password,omitempty"`
	PasswordConfirm string `json:"password_confirm,omitempty"`
}

func (u *NewUser) OK() error {
	if len(u.Username) == 0 {
		return ErrUsernameRequired
	} else if len(u.Password) == 0 {
		return ErrPasswordRequired
	} else if u.Password != u.PasswordConfirm {
		return ErrPasswordMismatch
	}
	return nil
}

func (u *NewUser) Create(sess *mgo.Session, user *User) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user = &User{Username: u.Username, PasswordHash: string(hash)}

	err = sess.DB("blue").C("users").Insert(user)
	if err != nil {
		if mgo.IsDup(err) {
			return ErrUseranmeAlreadyTaken
		}
		return err
	}

	return nil
}
