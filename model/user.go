package model

import (
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Username     string `json:"username" bson:"_id,omitempty"`
	Token        string `json:"token,omitempty"`
	PasswordHash string `json:"-"        bson:"password,omitempty"`
}

func (u *User) MatchPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(password))
}
